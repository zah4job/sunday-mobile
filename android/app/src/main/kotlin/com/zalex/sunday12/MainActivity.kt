package com.zalex.sunday12

import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant
import com.google.android.gms.ads.MobileAds

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        // val testDeviceIds = Arrays.asList("DA0D6555BD2998E79B1B8296AB452415")
        // val configuration = RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build()
        // MobileAds.setRequestConfiguration(configuration)

        // MobileAds.initialize(this) {}
    }
}
