import 'package:flutter/material.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:sunday12/network.dart';
import 'package:sunday12/pages/CodesPage.dart';
import 'package:sunday12/pages/DebugPage.dart';
import 'package:sunday12/pages/HqPage.dart';
import 'package:sunday12/pages/LoginPage.dart';
import 'package:sunday12/pages/new_agent/NewAgentPage.dart';
import 'package:sunday12/pages/RatingPage.dart';
import 'package:sunday12/pages/RegistrationPage.dart';
import 'package:sunday12/pages/ResultsPage.dart';
import 'package:sunday12/pages/RulesPage.dart';
import 'package:sunday12/pages/WrongCredentialsPage.dart';
import 'package:sunday12/theme/sunday_theme.dart';

import 'pages/StatusPage.dart';
import 'zDrawer.dart';

Future<void> main() async {
  await SentryFlutter.init(
    (options) {
      options.dsn =
          'https://cf6708edf70b4aeeb46ed330a468f6de@o1097080.ingest.sentry.io/6118286';
      // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
      // We recommend adjusting this value in production.
      options.tracesSampleRate = 1.0;
    },
    appRunner: () => runApp(MyApp()),
  );

  // or define SENTRY_DSN via Dart environment variable (--dart-define)
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

enum SundayPage {
  main,
  results,
  rating,
  codes,
  rules,
  hq,
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Hello World',
      theme: SundayTheme.lightTheme,
      initialRoute: '/newagent',
      routes: <String, WidgetBuilder>{
        '/main': (BuildContext context) => SundayHome(),
        '/results': (BuildContext context) => ResultsPage(),
        '/rating': (BuildContext context) => RatingRecordPage(),
        '/codes': (BuildContext context) => CodesPage(),
        '/rules': (BuildContext context) => RulesPage(),
        '/hq': (BuildContext context) => HqPage(),
        '/register': (BuildContext context) => RegistrationPage(),
        '/debug': (BuildContext context) => DebugPage(),
        '/login': (BuildContext context) => LoginPage(),
        '/newagent': (BuildContext context) => NewAgentPage(),
      },
    );
  }
}

class SundayHome extends StatefulWidget {
  const SundayHome({Key? key}) : super(key: key);

  @override
  _SundayHomeState createState() => _SundayHomeState();
}

class _SundayHomeState extends State<SundayHome> {
  void onTap() {
    print('on tap handler');
  }

  Future<bool> credentialsInStore() async {
    /// В хранилище есть логин и пароль
    if (await storageLogin() != '') {
      if (await storagePassword() != '') {
        return true;
      }
    }
    return false;
  }

  Future<Widget> getPage() async {
    /// Если есть логин + пароль - пытаемся их использовать
    /// Если ничего нет storage - считаем новым пользователем
    if (await credentialsInStore()) {
      if (await checkCredentials()) {
        return StatusPage(
          onTap: onTap,
        );
      }
    }
    return WrongCredentialsPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        // The title text which will be shown on the action bar
        title: Text('Sunday12: the one time'),
      ),
      body: StatusPage(
        onTap: onTap,
      ),
      drawer: zDrawer(context),
    );
  }
}
