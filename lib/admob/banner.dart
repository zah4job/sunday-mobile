import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class ZBanner {
  final BannerAd myBanner = BannerAd(
    adUnitId: 'ca-app-pub-3940256099942544/6300978111',
    size: AdSize.banner,
    request: AdRequest(),
    listener: BannerAdListener(),
  );

  Future<Widget> _adFuture() async {
    var x = 1;
    print('loading banner');
    await myBanner.load();
    return AdWidget(ad: myBanner);
  }

  FutureBuilder adFutureBuilder() {
    return FutureBuilder(
      future: _adFuture(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return snapshot.data as Widget;
        }
        if (snapshot.hasError) {
          return Text('error');
        }
        return CircularProgressIndicator();
      },
    );
  }

  void dispose() {
    myBanner.dispose();
  }

  Widget bannerBox() {
    return SizedBox(
      height: 50,
      width: 200,
      child: adFutureBuilder(),
    );
  }
}
