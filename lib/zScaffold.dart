import 'package:flutter/material.dart';
import 'package:sunday12/zDrawer.dart';

Scaffold zScaffold({
  required BuildContext context,
  required String title,
  required Widget body,
}) {
  return Scaffold(
    drawer: zDrawer(context),
    appBar: AppBar(
      // The title text which will be shown on the action bar
      title: Text(title),
    ),
    body: body,
  );
}
