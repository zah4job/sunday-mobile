import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sunday12/models/last_quest_result.dart';
import 'package:sunday12/network.dart';
import 'package:sunday12/zScaffold.dart';

class RightAnswer {
  final String text;
  final String imageUrl;
  final String rightAnswer;

  RightAnswer({
    Key? key,
    required this.text,
    required this.imageUrl,
    required this.rightAnswer,
  });
}

class ResultsPage extends StatefulWidget {
  const ResultsPage({Key? key}) : super(key: key);

  @override
  _ResultsPageState createState() => _ResultsPageState();
}

class _ResultsPageState extends State<ResultsPage> {
  double fontSize = 20;

  Future<LastQuestResult> futureResult() async {
    var response = await serverGet('api/quest/last-results/');
    if (response.statusCode != 200) {
      throw new Exception('server error with code: ${response.statusCode}');
    }
    var decoded = jsonDecode(response.body);
    return LastQuestResult.fromJson(decoded);
  }

  Widget wrongQuestionWidget(
      {required String text,
      required String image,
      required String yourAnswer,
      required String rightAnswer}) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 30,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(text),
          SizedBox(height: 15),
          Image.network(image),
          SizedBox(height: 15),
          Text('Ваш ответ: $yourAnswer'),
          SizedBox(height: 15),
          Text('Правильный ответ: $rightAnswer'),
        ],
      ),
    );
  }

  Widget firstElement(
      {required int questionCount, required DateTime questDate}) {
    if (questionCount > 0) {
      return Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
        ),
        child: Text('Неправильные ответы:'),
      );
    }
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Text(
            'Поздравляем, все ответы по миссии от ' +
                '${questDate.day}.' +
                '${questDate.month.toString().padLeft(2, '0')}.' +
                '${questDate.year.toString()}' +
                '  правильные!',
            style: TextStyle(fontSize: 18),
          )
        ],
      ),
    );
  }

  Widget lastElement() {
    return Center(
      child: OutlinedButton(
        child: Text('Посмотреть рейтинг'),
        onPressed: () => Navigator.pushNamed(context, '/rating'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'sunday12: последний квест',
      body: FutureBuilder(
        future: futureResult(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            LastQuestResult result = snapshot.data as LastQuestResult;
            List<WrongAnswer> results = result.wrongAnswers;
            return ListView.builder(
              itemBuilder: (context, index) {
                if (index == 0) {
                  return firstElement(
                    questionCount: results.length,
                    questDate: result.questInfo.startAt,
                  );
                } else if (index == results.length + 1) {
                  return lastElement();
                }

                WrongAnswer wrongQuestion = results[index - 1];
                return wrongQuestionWidget(
                  text: '$index. ${wrongQuestion.question.text}',
                  image: wrongQuestion.question.image,
                  yourAnswer: wrongQuestion.answer.isEmpty
                      ? '(пустой ответ)'
                      : wrongQuestion.answer,
                  rightAnswer: wrongQuestion.rightAnswer,
                );
              },
              itemCount: results.length + 2,
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
