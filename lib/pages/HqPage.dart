import 'package:flutter/material.dart';
import 'package:sunday12/zScaffold.dart';
import 'package:url_launcher/url_launcher.dart';

class HqPage extends StatelessWidget {
  const HqPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Sunday12: штаб',
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                '    Связь со штабом осуществляется в случае ' +
                    'крайней необходимости по адресу:',
              ),
              SizedBox(
                height: 20,
              ),
              TextButton(
                child: Text(
                  'support@sunday12.bizml.ru',
                  textAlign: TextAlign.left,
                ),
                onPressed: emailSupport,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void emailSupport() async {
    Uri uri = Uri(
        scheme: 'mailto',
        path: 'support@sunday12.bizml.ru',
        query: 'subject=Sunday support&body=App Version 3.23');

    await launch(uri.toString());
  }
}
