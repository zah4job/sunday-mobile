import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sunday12/activeQuiz.dart';
import 'package:sunday12/models/answered.dart';
import 'package:sunday12/models/quest.dart';
import 'package:sunday12/network.dart';
import 'package:sunday12/question_storage/storage.dart' as storage;

class StatusPage extends StatefulWidget {
  StatusPage({
    Key? key,
    required this.onTap,
  }) : super(key: key);
  final VoidCallback onTap;

  @override
  _StatusPageState createState() => _StatusPageState();
}

Widget noNextQuestWidget(BuildContext context) {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Column(
            children: [
              Text(
                'Следующий квест не найден.',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Дождитесь объявления результатов.',
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(
                height: 10,
              ),
              OutlinedButton(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/main');
                },
                child: Text('Обновить страницу'),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

class _StatusPageState extends State<StatusPage> {
  String status = 'unknown';
  void onPressed() {
    print('clicked');
    widget.onTap();
  }

  Widget childrenByStatus(Quest? quest) {
    if (status == 'active') {
      return ActiveQuiz(
        quest: quest as Quest,
        answered: quest.answered,
        onTimeOut: () => print('time runs out'),
      );
    }

    return Text('unknown widget for status: $status');
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchQuest(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error == 'no more quests') {
              return noNextQuestWidget(context);
            }
            if (snapshot.error is NetworkException) {
              if ((snapshot.error as NetworkException).cause == '403') {
                return Center(
                  child: OutlinedButton(
                    onPressed: () => Navigator.pushNamed(context, '/login'),
                    child: Text('На страницу входа'),
                  ),
                );
              }
            }

            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('${snapshot.error}'),
                  OutlinedButton(
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/main');
                    },
                    child: Text('Попробовать ещё раз'),
                  )
                ],
              ),
            );
          }
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          Quest quest = snapshot.data as Quest;
          DateTime now = DateTime.now();
          if (quest.startAt.isBefore(now) && quest.stopAt.isAfter(now)) {
            status = 'active';
          } else {
            status = 'wait';
          }
          return childrenByStatus(quest);
        });
  }

  @override
  void initState() {
    // updateQuest().then((isActive) {
    //   if (isActive) {
    //     setState(() {
    //       status = 'active';
    //     });
    //   } else {
    //     setState(() {
    //       status = 'waiting';
    //     });
    //   }
    // });
    super.initState();
  }

  /// Обновить Quest, если есть необходимость.
  /// Если обновлённый (по необходимости) квиз окажется активным - return true
  /// Если квеста нет или он не активен - return false
  Future<bool> updateQuest() async {
    Quest? lastQuest = await storage.loadQuest();
    var now = DateTime.now();
    if ((lastQuest == null) || (now.isAfter(lastQuest.stopAt))) {
      lastQuest = await fetchQuest();
    }

    return (lastQuest.startAt.isBefore(now) && lastQuest.stopAt.isAfter(now));
  }

  Future<Quest> fetchQuest() async {
    var response = await serverGet('api/quest/get-active/');
    if (response.statusCode == 404) {
      throw 'no more quests';
    }
    if (response.statusCode != 200) {
      throw NetworkException('${response.statusCode}');
    }
    var jsonResponse = jsonDecode(response.body);
    await Answered.loadFromStore();
    Quest quest = Quest.fromJson(jsonResponse, Answered.answered);
    storage.saveQuest(quest);
    return quest;
  }
}
