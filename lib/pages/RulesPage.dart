import 'package:flutter/material.dart';

import '../zScaffold.dart';

class RulesPage extends StatelessWidget {
  const RulesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Sunday12: правила',
      body: Text('Rules page'),
    );
  }
}
