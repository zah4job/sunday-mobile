import 'dart:math';

import 'package:flutter/material.dart';
import 'package:sunday12/zScaffold.dart';

const _chars = 'ABCDEF1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

class CodesPage extends StatefulWidget {
  const CodesPage({Key? key}) : super(key: key);

  @override
  _CodesPageState createState() => _CodesPageState();
}

class _CodesPageState extends State<CodesPage> {
  final Future<List<String>> _codes = Future<List<String>>.delayed(
    const Duration(seconds: 2),
    () => const <String>['123'],
  );

  List<Widget> _getCodes() {
    var widgets = <Widget>[];

    for (int i = 0; i < 5; i++) {
      widgets.add(Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 25.0,
          vertical: 8.0,
        ),
        child: Text('${i + 1}. ${getRandomString(8)}'),
      ));
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Sunday12: твои коды',
      body: Container(
        padding: EdgeInsets.all(20.0),
        alignment: Alignment.center,
        child: FutureBuilder(
          future: _codes,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return CircularProgressIndicator();
            } else {
              var codes = _getCodes();
              if (codes.length != 0) {
                return Text(
                  'У тебя нет доступных кодов',
                  style: TextStyle(fontSize: 20),
                );
              }
              return Column(
                children: [
                  Text(
                    'Поздравляем, теперь ты можешь пригласить ' +
                        'в наш секретный клуб пятерых участников',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: codes,
                  )
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
