import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:sunday12/zScaffold.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class TitleText extends Text {
  const TitleText(String text, {Key? key})
      : super(
          text,
          style: const TextStyle(fontSize: 20),
          key: key,
        );
}

class HintText extends Text {
  const HintText(String text, {Key? key})
      : super(
          text,
          style: const TextStyle(fontSize: 12),
          key: key,
        );
}

class _RegistrationPageState extends State<RegistrationPage> {
  bool nameError = true;
  String nameErrorText = '';
  bool codeError = true;
  String codeErrorText = '';
  bool phoneError = true;
  bool veriError = true;
  final nameController = TextEditingController();
  final codeController = TextEditingController();
  final numberController = TextEditingController();
  final veriController = TextEditingController();

  TextStyle textStyle1() {
    return TextStyle(fontSize: 20);
  }

  TextStyle textStyle2() {
    return TextStyle(fontSize: 12);
  }

  InputDecoration tfDecoration() {
    return InputDecoration(
      border: OutlineInputBorder(),
    );
  }

  Future<void> onOk() async {
    print('i am on buisness');
    var response = await http
        .post(Uri.parse('https://jsonplaceholder.typicode.com/posts'));

    if (response.statusCode == 201) {
      showDialog(
        context: context,
        builder: _showDialogOk,
      );
    } else {
      showDialog(
        context: context,
        builder: _showDialogError,
      );
    }

    setState(() {
      nameError = false;
      phoneError = false;
      codeError = false;
      veriError = false;
    });

    try {
      print('deviding by zero');
      throw new Exception('oshibka, witch.');
    } catch (exception, stackTrace) {
      print('senging to sentry...');
      await Sentry.captureException(exception, stackTrace: stackTrace);
      print('ok');
    }
  }

  SizedBox smallMargin() {
    return SizedBox(
      height: 10,
    );
  }

  SizedBox bigMargin() {
    return SizedBox(
      height: 20,
    );
  }

  void onChangeNumber(PhoneNumber newNumber) {
    print('new number is: $newNumber');
  }

  Widget phoneField() {
    return InternationalPhoneNumberInput(
      textFieldController: numberController,
      onInputChanged: onChangeNumber,
      hintText: '',
      selectorConfig: SelectorConfig(
        showFlags: false,
        selectorType: PhoneInputSelectorType.DIALOG,
      ),
      initialValue: PhoneNumber(isoCode: 'RU'),
      inputBorder: OutlineInputBorder(),
      onInputValidated: (numberIsValid) {
        if (numberController.text.length < 4) {
          numberIsValid = true;
        }
        setState(() {
          phoneError = !numberIsValid;
        });
      },
    );
  }

  @override
  void initState() {
    super.initState();
    nameController.addListener(validateName);
    codeController.addListener(validateCode);
    veriController.addListener(validateVeri);
  }

  @override
  void dispose() {
    nameController.dispose();
    codeController.dispose();
    numberController.dispose();
    veriController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Sunday12: результаты',
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                TitleText('Имя агента'),
                smallMargin(),
                TextField(
                  decoration: tfDecoration(),
                  controller: nameController,
                ),
                smallMargin(),
                nameHint(),
                bigMargin(),
                TitleText('Секретный код'),
                smallMargin(),
                TextField(
                  decoration: tfDecoration(),
                  controller: codeController,
                ),
                smallMargin(),
                codeHint(),
                bigMargin(),
                TitleText('Номер телефона'),
                phoneField(),
                smallMargin(),
                phoneHint(),
                bigMargin(),
                TitleText('Код подтверждения'),
                TextField(
                  decoration: tfDecoration(),
                  controller: veriController,
                ),
                smallMargin(),
                veriHint(),
                bigMargin(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    OutlinedButton(
                      onPressed: onOk,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Text(
                          'Я в деле!',
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Text nameHint() {
    if (nameError) {
      return Text(
        nameErrorText,
        style: TextStyle(color: Colors.redAccent),
      );
    }
    return Text('Имя будет отображаться в рейтинге');
  }

  Text codeHint() {
    if (codeError) {
      return Text(
        codeErrorText,
        style: TextStyle(color: Colors.redAccent),
      );
    }
    return Text('Код можно получить только у избранных');
  }

  Text phoneHint() {
    if (phoneError) {
      return Text(
        'Номер телефона указан неправильно',
        style: TextStyle(color: Colors.redAccent),
      );
    }
    return Text(
      'Туда будут отправляться инструции, как забрать приз',
    );
  }

  Text veriHint() {
    if (veriError) {
      return Text(
        'Код не верен или уже использован',
        style: TextStyle(color: Colors.redAccent),
      );
    }
    return Text(
      'Был отправлен по указанному номеру телефона',
    );
  }

  void validateName() {
    late bool errorValue;
    String text = nameController.text;
    late String errorText;
    errorValue = true;

    if (text.isEmpty) {
      errorText = 'Имя агента не может быть пустым';
    } else if (nameController.text.length > 10) {
      errorText = 'Имя не должно быть длинее 10 символов';
    } else {
      errorText = '';
      errorValue = false;
    }
    setState(() {
      nameError = errorValue;
      nameErrorText = errorText;
    });
  }

  void validateCode() {
    late String errorText;
    late bool isError;

    if (codeController.text.isEmpty) {
      errorText = '';
      isError = false;
    } else if (codeController.text.length != 8) {
      isError = true;
      errorText = 'Код должен содержать 8 символов';
    } else {
      isError = false;
      errorText = '';
    }
    setState(() {
      codeError = isError;
      codeErrorText = errorText;
    });
  }

  void validateVeri() {
    setState(() {
      veriError = true;
    });
  }
}

Widget _showDialogError(BuildContext context) {
  return _showDialog(context, 'Ошибка регистрации');
}

Widget _showDialogOk(BuildContext context) {
  return _showDialog(context, 'Регистрация прошла успешно');
}

Widget _showDialog(BuildContext context, String message) {
  return AlertDialog(
    title: Text(message),
    actions: <Widget>[
      TextButton(
        child: Text('Ok'),
        onPressed: () => Navigator.pop(context, 'ok'),
      )
    ],
  );
}
