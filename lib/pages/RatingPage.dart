import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sunday12/models/RatingRecord.dart' as model;

import '../network.dart';
import '../zScaffold.dart';

class RatingRecordPage extends StatefulWidget {
  const RatingRecordPage({Key? key}) : super(key: key);

  @override
  _RatingRecordPageState createState() => _RatingRecordPageState();
}

class _RatingRecordPageState extends State<RatingRecordPage> {
  late final Future<List<model.Record>> _futureRating;
  final refreshController = RefreshController(
    initialRefresh: true,
  );
  String? loadUrl = 'api/rating/';
  List<model.Record> rating = [];
  String myUserName = '';

  @override
  void initState() {
    _futureRating = fetchRating();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Sunday12: рейтинг',
      body: FutureBuilder(
        future: _futureRating,
        builder: ratingWidget,
      ),
    );
  }

  Widget userLineWidget(int index, String name, String myName, int rating) {
    String lineText = '${index + 1}. $name';
    return Container(
      color: name == myName ? Color(0xFFF4E10E) : Colors.white,
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: Text(lineText),
            ),
          ),
          Text('$rating')
        ],
      ),
    );
  }

  Widget ratingWidget(BuildContext context, AsyncSnapshot snapshot) {
    return SmartRefresher(
      controller: refreshController,
      enablePullUp: true,
      enablePullDown: false,
      onLoading: () async {
        await fetchRating();
        refreshController.loadComplete();
      },
      child: ListView.builder(
        itemBuilder: (context, index) {
          var item = rating[index];
          return Container(
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black26))),
              child: userLineWidget(
                  index, item.username, myUserName, item.rating));
        },
        itemCount: rating.length,
      ),
    );
  }

  Future<List<model.Record>> fetchRating() async {
    myUserName = await storageLogin();
    print('lets fetch...');
    if (loadUrl == null) {
      return [];
    }
    http.Response response = await serverGet(loadUrl!);
    var page = model.Page.fromJson(jsonDecode(response.body));
    rating.addAll(page.results);
    setState(() {
      if (page.next != null) {
        int start = page.next?.indexOf('/api/') ?? 0;
        String? nextUrl = page.next?.substring(start + 1);
        loadUrl = nextUrl as String;
      } else {
        print('this is last page');
        loadUrl = null;
      }
    });
    print('done...');
    return <model.Record>[];
  }
}
