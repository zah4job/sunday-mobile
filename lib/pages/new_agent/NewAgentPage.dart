import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:sunday12/pages/CodesPage.dart';
import 'package:sunday12/zScaffold.dart';

class NewAgentPage extends StatefulWidget {
  const NewAgentPage({Key? key}) : super(key: key);

  @override
  State<NewAgentPage> createState() => _NewAgentPageState();
}

class _NewAgentPageState extends State<NewAgentPage> {
  final controller = PageController(initialPage: 1);
  bool newAgent = false;

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Sunday12: твои коды',
      body: pageBody(),
    );
  }

  Widget pageBody() {
    return PageView(
      controller: controller,
      children: [
        whoAmI(),
        rules(),
      ],
    );
  }

  Widget whoAmI() {
    return Padding(
      padding: const EdgeInsets.all(40.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          OutlinedButton(
            child: Text('Я новый участник'),
            onPressed: () {
              newAgent = true;
            },
          ),
          SizedBox(
            height: 15,
          ),
          OutlinedButton(
            child: Text('Я уже регистрировался'),
            onPressed: () {
              newAgent = true;
            },
          ),
        ],
      ),
    );
  }

  Widget rules() {
    var splitter = SizedBox(
      height: 10,
    );
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('    Приветствую тебя в нашем закрытом клубе'),
            splitter,
            Text('    Первое правило клуба - рассказывать о клубе только' +
                ' лучшим друзьям и взрослым (в случае необходимости)'),
            splitter,
            Text('    Второе правило клуба -  каждый участник ' +
                'может привести в клуб только троих друзей не старше 16 лет.'),
            splitter,
            Text('    Третье правило клуба - вопросы появляются ' +
                'ровно в 12.00 каждый день, подведение итогов ' +
                'каждое воскресенье в 12.00'),
            splitter,
            Text('    Четвёртое правило клуба - отвечать на вопросы ' +
                'быстро и правильно'),
            splitter,
            Text('    Пятое правило клуба - есть только одна попытка дать ' +
                'правильный ответ.'),
            splitter,
            Text('    Шестое правило клуба - орфографические ошибки ' +
                'и неточности приравниваются к неправильному ответу.'),
            splitter,
            Text(
                '    Седьмое правило клуба - каждый месяц лучший имеет право ' +
                    'получить вознаграждение от штаба.'),
            splitter,
            Text('    Восьмое правило клуба - обращаться в штаб ' +
                'только в случае вопиющей несправедливости ' +
                'и технических сбоев.'),
            splitter,
            splitter,
            splitter,
            OutlinedButton(
              child: Text('Я в деле!'),
              onPressed: () {
                controller.jumpTo(2);
              },
            )
          ],
        ),
      ),
    );
  }
}
