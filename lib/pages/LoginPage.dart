import 'package:flutter/material.dart';
import 'package:sunday12/admob/banner.dart';
import 'package:sunday12/network.dart';
import 'package:sunday12/zScaffold.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final loginController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    storageLogin().then((value) {
      loginController.text = value;
    });
    storagePassword().then((value) {
      passwordController.text = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Sunday12: login',
      body: Column(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Login'),
                TextField(
                  controller: loginController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
                Text('Password'),
                TextField(
                  obscureText: true,
                  enableSuggestions: false,
                  autocorrect: false,
                  controller: passwordController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
                OutlinedButton(
                  onPressed: login,
                  child: Text('войти'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void login() async {
    saveCredentials(loginController.text, passwordController.text);
    bool isOk = await checkCredentials();
    if (isOk) {
      showDialog(context: context, builder: _okDialogBuilder);
    } else {
      showDialog(context: context, builder: _failDialogBuilder);
    }
  }

  Widget _okDialogBuilder(BuildContext context) {
    return buildDialog(context, 'Успешный вход');
  }

  Widget _failDialogBuilder(BuildContext context) {
    return buildDialog(context, 'Неверный логин-пароль');
  }

  Widget buildDialog(BuildContext context, String message) {
    return AlertDialog(
      title: Text(message),
      actions: <Widget>[
        TextButton(
          child: Text('Ok'),
          onPressed: () => Navigator.pop(context, 'ok'),
        )
      ],
    );
  }
}
