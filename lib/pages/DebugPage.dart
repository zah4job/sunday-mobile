import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sunday12/models/answered.dart';
import 'package:sunday12/network.dart';

import '../zScaffold.dart';

class DebugPage extends StatelessWidget {
  const DebugPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'debug page',
      body: Container(
        child: Column(
          children: [
            OutlinedButton(
              child: Text('debug me'),
              onPressed: onTap,
            ),
            OutlinedButton(
              onPressed: () => storageRead(),
              child: Text('read'),
            ),
            OutlinedButton(
              onPressed: () => storageWrite(),
              child: Text('write'),
            ),
            OutlinedButton(
              onPressed: () async {
                Answered.add(1);
                Answered.add(2);
                await Answered.saveToStore();
              },
              child: Text('write answered: [1, 2]'),
            ),
            OutlinedButton(
              onPressed: () async {
                Answered.answered = [];
                await Answered.saveToStore();
              },
              child: Text('write answered: []'),
            ),
            OutlinedButton(
              onPressed: () async {
                await Answered.loadFromStore();
              },
              child: Text('read answered'),
            )
          ],
        ),
      ),
    );
  }

  void onTap() async {
    try {
      http.Response response = await serverGet('api/');
      print(response.statusCode);
    } on Exception catch (_) {
      print('some error');
    }
    storageWrite();
    storageRead();

    // http.get(Uri.parse('www.ya.ru'));
  }
}
