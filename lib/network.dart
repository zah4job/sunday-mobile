import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

const String backend_uri = String.fromEnvironment(
  'BACKEND',
  defaultValue: 'https://sunday12.fun/',
);

class NetworkException implements Exception {
  String cause;
  NetworkException(this.cause);
}

// const String username = String.fromEnvironment(
//   'USERNAME',
//   defaultValue: 'noname',
// );
//
// const String password = String.fromEnvironment(
//   'PASSWORD',
//   defaultValue: '',
// );
Future<String> storageLogin() async {
  return await storage.read(key: 'login') ?? '';
}

Future<String> storagePassword() async {
  return await storage.read(key: 'password') ?? '';
}

/// Получение данных от сервера (backend)
Future<http.Response> serverGet(String uri) async {
  String myLogin = await storageLogin();
  String myPassword = await storagePassword();
  String credentials = base64Encode(utf8.encode('$myLogin:$myPassword'));

  var response = await http.get(
    Uri.parse(backend_uri + uri),
    headers: {HttpHeaders.authorizationHeader: ('Basic ' + credentials)},
  ).timeout(const Duration(seconds: 3));

  return response;
}

Future<http.Response> serverPost(String uri,
    {Map<String, String> parameters: const {}}) async {
  String myLogin = await storageLogin();
  String myPassword = await storagePassword();
  String credentials = base64Encode(utf8.encode('$myLogin:$myPassword'));

  var response = await http
      .post(Uri.parse(backend_uri + uri),
          headers: {
            HttpHeaders.authorizationHeader: ('Basic ' + credentials),
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(parameters))
      .timeout(const Duration(seconds: 3));

  return response;
}

FlutterSecureStorage storage = FlutterSecureStorage();

void storageWrite() async {
  var random = Random();
  int randomValue = random.nextInt(10);
  await storage.write(key: 'key', value: randomValue.toString());
  print('writed: $randomValue');
}

void storageRead() async {
  String? storageValue = await storage.read(key: 'key');
  String value = storageValue ?? 'empty';
  print(value);
}

void saveCredentials(String username, String password) async {
  await storage.write(key: 'login', value: username);
  await storage.write(key: 'password', value: password);
}

Future<bool> checkCredentials() async {
  /// Проверка валидности введённый данных.
  try {
    http.Response response = await serverGet('api/');
    return response.statusCode == 200;
  } catch (exception) {
    print(exception);
    return false;
  }
}
