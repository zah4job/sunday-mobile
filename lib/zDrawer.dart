import 'package:flutter/material.dart';

Widget zDrawer(BuildContext context) {
  return Theme(
    data: Theme.of(context).copyWith(
        textTheme: TextTheme(
      bodyText1: TextStyle(
          fontSize: 20, color: Colors.black, fontFamily: 'Philosopher'),
    )),
    child: Drawer(
      child: ListView(
        children: [
          Container(
            height: 100,
            alignment: Alignment.topLeft,
            padding: EdgeInsets.all(10),
            child: Image.asset(
              'images/sun.png',
              width: 80,
            ),
          ),
          ListTile(
            title: Text(
              'Главная страница',
            ),
            onTap: () => Navigator.pushNamed(context, '/main'),
          ),
          ListTile(
            title: Text('Результаты'),
            onTap: () => Navigator.pushNamed(context, '/results'),
          ),
          ListTile(
            title: Text('Рейтинг'),
            onTap: () => Navigator.pushNamed(context, '/rating'),
          ),
          ListTile(
            title: Text('Мои коды'),
            onTap: () => Navigator.pushNamed(context, '/codes'),
          ),
          ListTile(
            title: Text('Правила'),
            onTap: () => Navigator.pushNamed(context, '/rules'),
          ),
          ListTile(
            title: Text('Штаб'),
            onTap: () => Navigator.pushNamed(context, '/hq'),
          ),
          ListTile(
            title: Text('Логин'),
            onTap: () => Navigator.pushNamed(context, '/login'),
          ),
          ListTile(
            title: Text('Debug'),
            onTap: () => Navigator.pushNamed(context, '/debug'),
          ),
        ],
      ),
    ),
  );
}
