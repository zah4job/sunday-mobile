import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sunday12/models/answered.dart';
import 'package:sunday12/models/quest.dart';

FlutterSecureStorage storage = FlutterSecureStorage();

void saveQuest(Quest quest) async {
  var data2Save = jsonEncode(quest.toJson());
  await storage.write(key: 'quest', value: data2Save);
  await storage.containsKey(key: 'quest');
}

Future<bool> haveQuest() async {
  var loadedQuest = await _loadQuest();
  return loadedQuest != null;
}

Future<Quest?> loadQuest() async {
  var quest = await _loadQuest();
  return quest;
}

Future<Quest?> _loadQuest() async {
  bool haveData = await storage.containsKey(key: 'quest');
  if (!haveData) {
    return null;
  }
  var storageData = await storage.read(key: 'quest');
  var decodedData = jsonDecode(storageData as String);
  await Answered.loadFromStore();
  return Quest.fromJson(decodedData, Answered.answered);
}
