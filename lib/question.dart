import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sunday12/models/answered.dart';
import 'package:sunday12/network.dart';

class Question extends StatefulWidget {
  final String text;
  final String imageUrl;
  final int questionId;
  final VoidCallback onAnswer;

  const Question({
    Key? key,
    required this.onAnswer,
    this.text = 'default text',
    this.imageUrl = 'https://dummyimage.com/600x249/000/fab',
    this.questionId = 0,
  }) : super(key: key);

  @override
  _QuestionState createState() => _QuestionState();
}

enum Status {
  initial,
  sending,
  done,
  alreadyReported,
  error,
}

class _QuestionState extends State<Question> {
  String answer = '';
  Status status = Status.initial;
  TextEditingController textController = TextEditingController();

  String statusString() {
    switch (this.status) {
      case Status.initial:
        return 'Отправить ответ';
      case Status.sending:
        return 'Отправка...';
      case Status.alreadyReported:
        return 'Отправлен ранее';
      case Status.error:
        return 'Ашипка';
      case Status.done:
        return 'Доставлен';
      default:
        return 'Штош...${this.status}';
    }
  }

  Future<Status> postAnswer() async {
    print('answer is: $answer}');
    String uri = 'api/question/${widget.questionId}/post-answer/';
    late http.Response response;
    try {
      response = await serverPost(Uri.encodeFull(uri),
          parameters: {'answer': '$answer'});
    } on TimeoutException {
      return Status.error;
    }

    print('status code: ${response.statusCode}');
    if (response.statusCode == 201) {
      return Status.done;
    } else if (response.statusCode == 409) {
      return Status.alreadyReported;
    } else {
      return Status.error;
    }
  }

  void onPressed() {
    if (status != Status.initial) {
      print('answer can be sended only in initial state');
      return;
    }
    print('sending...');
    setState(() {
      status = Status.sending;
    });
    postAnswer().then((value) {
      Answered.add(widget.questionId);
      Answered.saveToStore();
      widget.onAnswer();
      setState(() {
        status = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              widget.text,
              style: TextStyle(fontSize: 20),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Image.network(widget.imageUrl),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              'Ответ:',
              style: TextStyle(fontSize: 22),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: TextField(
            onChanged: (newValue) {
              setState(() {
                answer = newValue;
              });
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              // labelText: 'Введите ответ',
            ),
            controller: textController,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: SizedBox(
            width: double.infinity,
            child: OutlinedButton(
                onPressed: activeSendButton() ? onPressed : null,
                child: Text(statusString())),
          ),
        ),
      ],
    );
  }

  bool activeSendButton() {
    if (textController.text.isEmpty) {
      return false;
    }
    return status == Status.initial;
  }
}
