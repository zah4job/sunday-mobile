import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sunday12/components/next_quest_timer.dart';
import 'package:sunday12/components/quest_results_timer.dart';

import 'models/quest.dart';
import 'network.dart' as network;
import 'question.dart';

class ActiveQuiz extends StatefulWidget {
  final Quest quest;
  final List<int> answered;
  final VoidCallback onTimeOut;
  const ActiveQuiz(
      {required this.quest,
      required this.answered,
      required this.onTimeOut,
      Key? key})
      : super(key: key);

  @override
  _ActiveQuizState createState() => _ActiveQuizState();
}

class _ActiveQuizState extends State<ActiveQuiz> {
  Timer? _timer;
  double timeSpend = 0; // прошло времени с начала задания в секунду
  int timeLast = 100500; // времени осталось до конца задания
  int hours = 0;
  int minutes = 0;
  int seconds = 0;
  late Timer timer;

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    var now = DateTime.now();
    timeSpend = now.difference(widget.quest.startAt).inMilliseconds / 1000.0;
    timeLast = widget.quest.stopAt.difference(now).inSeconds;
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      return;
      setState(() {
        timeSpend += 1;
        timeLast -= 1;
        int iSeconds = timeSpend.toInt();
        seconds = iSeconds % 60;
        minutes = ((iSeconds ~/ 60)) % 60;
        hours = iSeconds ~/ 3600;

        if (timeLast <= 0) {
          timer.cancel();
          widget.onTimeOut();
        }
      });
    });
    super.initState();
  }

  Widget timerWidget() {
    var sMinutes = minutes.toString().padLeft(2, '0');
    var sSeconds = seconds.toString().padLeft(2, '0');
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 20),
      child: Text(
        '$hours:$sMinutes:$sSeconds',
        style: TextStyle(
          fontFamily: 'RobotoMono',
          fontSize: 35,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (notAllAnswered()) {
      return ListView.builder(
        itemCount: widget.quest.questions.length + 1,
        itemBuilder: (context, index) {
          if (index == 0) {
            return timerWidget();
          }
          Questions question = widget.quest.questions[index - 1];
          if (widget.answered.contains(question.id)) {
            return Text('Ты уже ответил на вопрос: ${question.id}');
          }
          return Question(
            text: '$index. ${question.text}',
            imageUrl: network.backend_uri + question.image.substring(1),
            questionId: question.id,
            onAnswer: () => {widget.answered.add(question.id)},
          );
        },
      );
    } else {
      return Container(
        alignment: Alignment.topCenter,
        child: Column(
          children: [
            NextQuestTimer(),
            QuestResultsTimer(
              targetTime: widget.quest.stopAt,
            ),
          ],
        ),
      );
    }
  }

  bool notAllAnswered() {
    /// true - если есть неотвеченные вопросы
    /// false - если уже отвечать нечего
    for (int i = 0; i < widget.quest.questions.length; i++) {
      var question = widget.quest.questions[i];
      if (!widget.answered.contains(question.id)) {
        return true;
      }
    }
    return false;
  }

  void startTimer() {
    if (_timer == null) {
      _timer = new Timer.periodic(Duration(seconds: 1), (timer) {
        return;
        print('$timeSpend');
        setState(() {
          timeSpend += 1;
          seconds = (timeSpend % 60).round();
          minutes = timeSpend % 3600 ~/ 60;
          hours = timeSpend ~/ 3600;
        });
      });
    }
  }

  void stopTimer() {
    _timer?.cancel();
    _timer = null;
  }
}
