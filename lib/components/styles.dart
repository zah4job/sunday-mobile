import 'package:flutter/material.dart';

const TextStyle kTimerTextStyle = TextStyle(
  fontFamily: 'RobotoMono',
  fontSize: 30,
);

const TextStyle kTimerLabelTextStyle = TextStyle(
  fontSize: 18,
);
