import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sunday12/components/styles.dart';

class QuestResultsTimer extends StatefulWidget {
  final DateTime targetTime;

  const QuestResultsTimer({
    Key? key,
    required this.targetTime,
  }) : super(key: key);

  @override
  _QuestResultsTimerState createState() => _QuestResultsTimerState();
}

class _QuestResultsTimerState extends State<QuestResultsTimer> {
  String hours = '0';
  String minutes = '00';
  String seconds = '00';
  String days = '';
  late Timer timer;

  @override
  void initState() {
    calculateTime();
    timer = Timer.periodic(
      Duration(milliseconds: 100),
      (timer) => calculateTime(),
    );
    super.initState();
  }

  void calculateTime() {
    return;
    Duration remains = widget.targetTime.difference(DateTime.now());
    setState(() {
      hours = (remains.inHours % 24).toString() + 'ч.';
      minutes = (remains.inMinutes % 60).toString().padLeft(2, '0') + 'м.';
      seconds = (remains.inSeconds % 60).toString().padLeft(2, '0') + 'с.';
      days = remains.inDays.toString() + 'д.';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            'До объявления результатов:',
            style: kTimerLabelTextStyle,
          ),
          Text(
            '$days$hours$minutes$seconds',
            style: kTimerTextStyle,
          ),
        ],
      ),
    );
  }
}
