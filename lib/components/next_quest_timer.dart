import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sunday12/models/next_quest.dart';
import 'package:sunday12/network.dart';

import 'styles.dart';

class NextQuestTimer extends StatefulWidget {
  const NextQuestTimer({Key? key}) : super(key: key);

  @override
  _NextQuestTimerState createState() => _NextQuestTimerState();
}

class _NextQuestTimerState extends State<NextQuestTimer> {
  String days = '';
  String hours = '0';
  String minutes = '00';
  String seconds = '00';
  DateTime nextQuestTime = DateTime(1970);
  Timer? timer;

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void calculateTime() {
    return;
    Duration delta = nextQuestTime.difference(DateTime.now());
    setState(() {
      days = delta.inDays.toString() + 'д.';
      hours = (delta.inHours % 24).toString() + 'ч.';
      minutes = (delta.inMinutes % 60).toString().padLeft(2, '0') + 'м.';
      seconds = (delta.inSeconds % 60).toString().padLeft(2, '0') + 'c.';
    });
  }

  Future<NextQuest> fetchNextQuest() async {
    var response = await serverGet('api/quest/next-date/');
    if (response.statusCode != 200) {
      throw Exception('Response with code: ${response.statusCode}');
    }
    var decoded = jsonDecode(response.body);
    var nextQuest = NextQuest.fromJson(decoded);
    nextQuestTime = nextQuest.startAt;
    timer = Timer.periodic(
      Duration(milliseconds: 100),
      (timer) => calculateTime(),
    );
    return nextQuest;
  }

  late final Future<NextQuest> _nextQuest;

  @override
  void initState() {
    _nextQuest = fetchNextQuest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.all(20),
      child: FutureBuilder(
        future: _nextQuest,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return errorWidget(snapshot.error.toString());
          }
          if (snapshot.hasData) {
            return body(snapshot.data as NextQuest);
          }
          return CircularProgressIndicator();
        },
      ),
    );
  }

  Column body(NextQuest nextQuest) {
    /// Содержание страницы после успешной загрузки данных
    return Column(
      children: [
        Text(
          'Все задания выполнены, до следующей встречи',
          style: kTimerLabelTextStyle,
          textAlign: TextAlign.center,
        ),
        Text(
          '$days$hours$minutes$seconds',
          style: kTimerTextStyle,
        ),
      ],
    );
  }

  Widget errorWidget(String message) {
    return Column(
      children: [
        Text(message),
        OutlinedButton(
          onPressed: () => Navigator.pushReplacementNamed(context, '/main'),
          child: Text('Попробовать ещё раз'),
        )
      ],
    );
  }
}
