import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

FlutterSecureStorage storage = FlutterSecureStorage();

class Answered {
  static List<int> answered = [];
  static String storageKey = 'answered';

  static void add(int questionId) {
    if (!answered.contains(questionId)) {
      answered.add(questionId);
      print('added to answered: $questionId');
    } else {
      print('$answered already in store');
    }
  }

  static bool have(int questionId) {
    return answered.contains(questionId);
  }

  static Future<void> saveToStore() async {
    answered = answered.toSet().toList();
    await storage.write(key: 'answered', value: jsonEncode(answered));
  }

  static Future<void> loadFromStore() async {
    if (await storage.containsKey(key: storageKey)) {
      var answeredString = await storage.read(key: storageKey) as String;
      answered = (jsonDecode(answeredString) as List<dynamic>).cast<int>();
      answered = answered.toSet().toList();
    }
    print('loaded answered: $answered');
  }
}
