import 'package:flutter/material.dart';

class QuestionModel {
  final int id;
  final String text;
  final String imageUri;

  QuestionModel({
    Key? key,
    this.id = 1,
    this.text = 'default text',
    this.imageUri = 'https://dummyimage.com/600x251/000/fab',
  });
}
