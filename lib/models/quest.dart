class Quest {
  Quest({
    required this.id,
    required this.startAt,
    required this.stopAt,
    required this.description,
    required this.season,
    required this.questions,
  });
  late final int id;
  late final DateTime startAt;
  late final DateTime stopAt;
  late final String description;
  late final int season;
  late final List<Questions> questions;
  late final List<int> answered;

  Quest.fromJson(Map<String, dynamic> json, List<int> answeredIds) {
    id = json['id'];
    startAt = DateTime.parse(json['start_at']);
    stopAt = DateTime.parse(json['stop_at']);
    description = json['description'];
    season = json['season'];
    questions =
        List.from(json['questions']).map((e) => Questions.fromJson(e)).toList();
    answered = answeredIds;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['start_at'] = startAt.toString();
    _data['stop_at'] = stopAt.toString();
    _data['description'] = description;
    _data['season'] = season;
    _data['questions'] = questions.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Questions {
  Questions({
    required this.id,
    required this.image,
    required this.text,
  });
  late final int id;
  late final String image;
  late final String text;

  Questions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['image'] = image;
    _data['text'] = text;
    return _data;
  }
}
