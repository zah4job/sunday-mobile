import 'dart:convert';

class QuestInfo {
  QuestInfo({
    required this.startAt,
  });
  late final DateTime startAt;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{'start_at': jsonEncode(startAt)};
  }

  QuestInfo.fromJson(Map<String, dynamic> json) {
    startAt = DateTime.parse(json['start_at']);
  }
}

class LastQuestResult {
  LastQuestResult({
    required this.questInfo,
    required this.wrongAnswers,
  });
  late final QuestInfo questInfo;
  late final List<WrongAnswer> wrongAnswers;

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['quest'] = jsonEncode(questInfo);
    _data['wrong_answers'] = jsonEncode(wrongAnswers);
    return _data;
  }

  LastQuestResult.fromJson(Map<String, dynamic> json) {
    questInfo = QuestInfo.fromJson(json['quest']);
    wrongAnswers = List<WrongAnswer>.from(
      json['wrong_answers'].map((item) => WrongAnswer.fromJson(item)),
    );
  }
}

class WrongAnswer {
  WrongAnswer({
    required this.id,
    required this.question,
    required this.answer,
    required this.rightAnswer,
  });
  late final int id;
  late final Question question;
  late final String answer;
  late final String rightAnswer;

  WrongAnswer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    question = Question.fromJson(json['question']);
    answer = json['answer'];
    rightAnswer = json['right_answer'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['question'] = question.toJson();
    _data['answer'] = answer;
    _data['right_answer'] = rightAnswer;
    return _data;
  }
}

class Question {
  Question({
    required this.id,
    required this.image,
    required this.text,
  });
  late final int id;
  late final String image;
  late final String text;

  Question.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['image'] = image;
    _data['text'] = text;
    return _data;
  }
}
