class NextQuest {
  NextQuest({
    required this.startAt,
  });
  late final DateTime startAt;

  NextQuest.fromJson(Map<String, dynamic> json) {
    startAt = DateTime.parse(json['start_at']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['start_at'] = startAt.toString();
    return _data;
  }
}
