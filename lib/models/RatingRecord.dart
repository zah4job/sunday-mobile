class Page {
  Page({
    required this.count,
    required this.next,
    this.previous,
    required this.results,
  });
  late final int count;
  late final String? next;
  late final Null previous;
  late final List<Record> results;

  Page.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    next = json['next'];
    previous = null;
    results =
        List.from(json['results']).map((e) => Record.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['count'] = count;
    _data['next'] = next;
    _data['previous'] = previous;
    _data['results'] = results.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Record {
  Record({
    required this.id,
    required this.username,
    required this.rating,
  });
  late final int id;
  late final String username;
  late final int rating;

  Record.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['username'] = username;
    _data['rating'] = rating;
    return _data;
  }
}
