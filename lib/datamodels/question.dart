class QuestionModel {
  final int id;
  final Uri? imageUri;
  final String questionText;

  QuestionModel({
    required this.id,
    required this.imageUri,
    required this.questionText,
  });
}
