import 'package:flutter/material.dart';

class SundayTheme {
  static ThemeData get lightTheme {
    return ThemeData(
      primarySwatch: Colors.yellow,
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          overlayColor: MaterialStateColor.resolveWith((states) {
            if (states.contains(MaterialState.pressed)) {
              return Color(0x5fBBAA04);
            }
            return Colors.white;
          }),
          // foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
          foregroundColor: MaterialStateColor.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) return Colors.grey;
            return Colors.black;
          }),
          backgroundColor: MaterialStateColor.resolveWith((states) {
            if (states.contains(MaterialState.disabled))
              return Color(0xfff6f6f6);
            return Colors.yellow;
          }),
        ),
      ),
      buttonTheme: ButtonThemeData(
        buttonColor: Colors.red,
        focusColor: Colors.black,
        splashColor: Colors.red,
      ),
      fontFamily: 'Philosopher',
    );
  }
}
