// Этот тест - демонстрация. Будут другие - можно его удалить.
import 'package:flutter_test/flutter_test.dart';
import 'package:sunday12/main.dart';

void main() {
  testWidgets('Header test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('Sunday12: the one time'), findsOneWidget);
  });
}
